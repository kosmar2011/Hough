# Ηough
Hough Transform in C for Catapult HLS

For the algorithm to work correctly you need to perform Canny Edge Detection in the image first. I do that with Matlab and I specify on which image I perform the algorithm in run.sh. 

C++ Code is synthesized in Catapult and the algorithm works correctly. Pipelining and Unrolling to be implemented.
